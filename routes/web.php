<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/khanh',function(){
	return view('admin.layout.index');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix'=>'admin'],function(){

   
    Route::group(['prefix'=>'Transfer'],function(){
    	Route::get('getTransfer','TransferController@getTransfer')->name('getTransfer');
    	Route::post('postTransfer','TransferController@postTransfer')->name('postTransfer');
    });
	Route::group(['prefix'=>'User','middleware'=> 'checklogout'],function(){
		Route::get('list_user','UserController@index')->name('list_user');
		Route::get('create_user','UserController@create')->name('create_user');
		Route::post('store_user','UserController@store')->name('store_user');
		Route::get('edit_user/{id}','UserController@edit')->name('edit_user');
		Route::post('update_user/{id}','UserController@update')->name('update_user');
		Route::get('destroy_user/{id}','UserController@destroy')->name('destroy_user');
		Route::get('view/{id}','UserController@view')->name('view');
	});
	Route::group(['prefix'=>'Wallet'],function(){
		Route::get('list_wallet','WalletController@index')->name('list_wallet');
		Route::get('create_wallet','WalletController@create')->name('create_wallet')->middleware('ViewDelvi');
		Route::post('store_wallet','WalletController@store')->name('store_wallet');
		Route::get('edit_wallet/{id}','WalletController@edit')->name('edit_wallet');
		Route::post('update_wallet/{id}','WalletController@update')->name('update_wallet');
		Route::get('destroy_wallet/{id}','WalletController@destroy')->name('destroy_wallet');
	});
	Route::group(['prefix'=>'Expend'],function(){
		Route::get('giao_dich','ExpendController@create')->name('create');
		Route::post('giao_dich','ExpendController@store')->name('store');
		Route::get('list_chi','ExpendController@listchi')->name('listchi');
		Route::get('list_thu','ExpendController@listthu')->name('listthu');
		Route::get('list_days','ExpendController@listdays')->name('listdays');
		Route::get('list_day/{month}','ExpendController@listDayForMonth')->name('listDayForMonth');
		Route::get('day/{day}','ExpendController@day')->name('day');
		Route::get('list_chi/excel','ExpendController@excel')->name('excel');
		Route::get('list_thu/excel','ExpendController@excels')->name('excels');
		
	});
});
Route::get('metronic',function(){
	return view('admin.layout.index');
});
Route::get('/home','LoginController@home')->name('home');
Route::get('/login','LoginController@getLogin')->middleware('checklogin')->name('get_login');
Route::post('/login','LoginController@postLogin')->name('postLogin');
Route::get('/logout','LoginController@logout')->name('logout');
Route::get('/reset','LoginController@reset')->name('reset');
Route::post('/reset','LoginController@resetPassword')->name('resetPassword');
Route::get('/register','LoginController@create')->name('create_register');
Route::post('/register','LoginController@store')->name('store_register');
