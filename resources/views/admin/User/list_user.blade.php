 @extends('admin.layout.index_metronic')
@section('metronic')
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header"> Danh sách tài khoản

        </h1>
        <h4>

          <a class="btn btn-success" href="{{route('getTransfer')}}">Chuyển tiền </a>
          <a class="btn btn-danger" href="{{route('create')}}">Giao dịch </a>
        </h4>

      </div>
      <table class="table table-bordered">
        <thead>
         <tr align="center">
          <th style="text-align: center;">STT</th>
          <th style="text-align: center;">Tên Tài khoản</th>
          <th style="text-align: center;">Email</th>
          <th style="text-align: center;">Sô tài khoản</th>
          <th style="text-align: center;">Thay đổi thông tin</th>
          <th style="text-align: center;">Xem thông tin</th>

        </tr>
      </thead>
      <tbody>

       <tr class="odd gradeX"  align="center">
        <td>{{1}}</td>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td>{{$user->code}}</td>

        <td class="center"><i class="fa fa-edit  fa-fw"></i><a href="{{route('edit_user',$user->id)}}">Thay đổi thông tin tài khoản</a></td>
        <td class="center" ><i class="fa fa-file-check fa-fw"></i><a href="{{route('view',$user->id)}}">Thông tin tài khoản</a></td>
      </tr>

    </tbody>
  </table>

</div>



</div>
</div>

@stop 
