@extends('admin.layout.index_metronic')
@section('metronic')
<div id="page-wrapper" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tài khoản
                    <small>Thay đổi thông tin</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">

                @if(session('thongbao'))
                <div class="alert alert-success">
                    {{session('thongbao')}}
                </div>
                 @endif
                <form action="{{route('update_user',$user->id)}}" method="POST" enctype="multipart/form-data">
                 @csrf()
                 <div class="form-group">
                    <label for="name" >Ten</label>
                    <input id="name" class="form-control" name="name" placeholder="Ten " value="{{$user->name}}" />
                    <div style="margin-top:10px">
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" >Email</label>
                    <input id="email" class="form-control" name="email" placeholder="email" value="{{$user->email}}"  />
                    <div style="margin-top:10px">
                     @error('email')
                         <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                    </div>
                     
                </div>
                <div class="form-group">
                    <label for="code" >Số tài khoản</label>
                    <input id="code" class="form-control" disabled="code" name="code" placeholder="code" value="{{$user->code}}"  />

                </div>
                <div class="form-group">
                    <label for="password">Password </label>
                    <input id="password" class="form-control" disabled="password" type="password" name="password" placeholder="Nhap password" value="{{$user->password}}"  />

                </div>
                <div class="form-group">
                    <label for="images" >Anh</label>
                    <input  id="images" class="form-control" type="file" name="images"  value="{{$user->images}}" />
                    <div style="margin-top:10px" >
                     @error('images')
                         <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                    </div>
                     
                  </div>
                <div class="form-group">
                    <label for="birthday" >Birthday</label>
                    <input id="birthday" class="form-control" type="date" name="birthday" placeholder="Birthday" value="{{$user->birthday}}" />
                    <div style="margin-top:10px">
                    @error('birthday')
                         <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                    </div>
                     
                </div>

            <button type="submit" class="btn btn-primary">Thay đổi thông tin tài khoản</button>
            <a  class="btn btn-primary" href="{{route('list_user')}}">Back</a>
            <form>
            </div>
        </div>

    </div>

</div>


</div>
@stop
