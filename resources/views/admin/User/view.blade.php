@extends('admin.layout.index_metronic')
@section('metronic')
<div id="page-wrapper" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thông tin tài khoản
                  
                </h1>
               
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
               
                
                <form action="" method="POST" enctype="multipart/form-data">
               
                 <div class="form-group">
                    <label for="name">Ten</label>
                    <input id="name" class="form-control" name="name" placeholder="Ten " value="{{$user->name}}" />
                   
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" class="form-control" name="email" placeholder="email" value="{{$user->email}}"  />
                    
                </div>
                <div class="form-group">
                    <label for="code">Số tài khoản</label>
                    <input id="code" class="form-control" disabled="code" name="code" placeholder="code" value="{{$user->code}}"  />
                   
                </div>
                
                @if($user->images)
                  <div class="form-group">
                    <label for="images">Anh</label>
                    <div>
                        <img style="border-radius: 50%" width="200px" height="200px" src="{{url('storage/'.$user->images)}}" >
                    </div>
                  </div>
                @else

                @endif  
                <div class="form-group">
                    <label for="birthday">Birthday</label>
                    <input id="birthday" class="form-control"  name="birthday" placeholder="Birthday" value="{{$user->birthday}}"  />
                    
                </div>
                <a class="btn btn-primary"  href="{{route('list_user')}}"> Back </a>
            <form>
            </div>
        </div>
     
    </div>
 
</div>


</div>
@stop