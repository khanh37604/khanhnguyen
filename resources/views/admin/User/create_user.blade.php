@extends('admin.layout.index_metronic')
@section('metronic')
<div id="page-wrapper" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">User
                    <small>add</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @if(session('thongbao'))
                <div class="alert alert-success" >
                    {{session('thongbao')}}
                </div>
                @endif
                <form action="{{route('store_user')}}" method="POST">
                 @csrf()
                 <div class="form-group ">
                    <label for="name" >Ten</label>
                    <input id="name"  class="form-control" name="name" placeholder="Ten "   />
                     @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email"  class="form-control" name="email" placeholder="email"  />
                     @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror     
                </div>
                <div class="form-group">
                    <label for="password" >Password </label>
                    <input id="password" class="form-control" type="password" name="password" placeholder="Nhap password"  />
                    <div style="margin-top:10px" >
                        @error('password')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                      
                </div>
                  <div class="form-group">
                    <label for="password">Password </label>
                    <input id="password" class="form-control" type="password" name="password" placeholder="Nhap password"  />
                    <div style="margin-top:10px" >
                    @error('password')
                        <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Thêm tài khoản</button>       
                <form>
                </div>
                
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

</div>
@stop