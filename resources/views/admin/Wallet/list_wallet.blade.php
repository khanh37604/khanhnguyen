@extends('admin.layout.index_metronic')
@section('metronic')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Danh sách ví
                </h1>
                <div class="col-lg-4">
                    @if(session('danger'))
                    <div class="alert alert-danger">
                        {{session('danger')}}
                    </div>
                    @endif
                </div>

                @if($wallet)
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr align="center">
                        <th style="text-align: center;">STT</th>
                        <th style="text-align: center;">Name</th>
                        <th style="text-align: center;">Số tiền</th>
                        <th style="text-align: center;">Sửa ví</th>
                        <th style="text-align: center;">Xóa ví</th>
                    </tr>
                </thead>
                <tbody>

                    <tr class="odd gradeX" align="center">
                        <td>1</td>
                        <td>{{$wallet->name }}</td>
                        <td>{{$wallet->money}}</td>
                        <td class="center"><i class="fa fa-edit fa-fw "></i></i><a
                                href="{{route('edit_wallet',$wallet->id)}}">Sửa thông tin ví</a></td>
                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a
                                href="{{route('destroy_wallet',$wallet->id)}}">Xóa ví</a></td>
                    </tr>
                    @else
                    <div>
                        <h3>
                            Tài khoản chưa tạo ví,vui lòng thêm ví
                        </h3>
                    </div>
                    @endif
                </tbody>
            </table>

        </div>
    </div>
</div>

@stop