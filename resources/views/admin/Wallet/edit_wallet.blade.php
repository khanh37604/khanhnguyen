@extends('admin.layout.index_metronic')
@section('metronic')
<div id="page-wrapper" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Ví
                    <small>Thay đổi thông tin</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
               
                @if(session('thongbao'))
                <div class="alert alert-success">
                    {{session('thongbao')}}
                </div>
                @endif
                <form action="{{route('update_wallet',$wallet->id)}}" method="POST" >
                 @csrf()
                 <div class="form-group">
                    <label for="name">Ten ví</label>
                    <input id="name" class="form-control" name="name" placeholder="Ten " value="{{$wallet->name}}" />
                   @error('name')
                    <div class="alert alert-success" >
                        {{$message}}
                    </div>
                   @enderror 
                </div>
                <div class="form-group">
                    <label for="money" >Số tiền</label>
                    <input id="money" class="form-control" name="money" placeholder="Money" value="{{$wallet->money}}"  />
                    @error('money')
                    <div class="alert alert-success" >
                        {{$message}}
                    </div>
                    @enderror
                </div>
              
                
            <button type="submit" class="btn btn-primary">Thay đổi thông tin ví</button>
            <a  class="btn btn-primary" href="{{route('list_wallet')}}">Back</a>
            
            <form>
            </div>
        </div>
     
    </div>
 
</div>


</div>
@stop