@extends('admin.layout.index_metronic')
@section('metronic')
<div id="page-wrapper" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Vi tai khoan
                    <small>add</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
               
                <!-- @if(session('thongbao'))
                <div class="alert alert-success">
                  {{session('thongbao')}}
                </div>
                @endif -->
               
                <form action="{{route('store_wallet')}}" method="POST">
              {{csrf_field()}}
                 <div class="form-group">
                    <label for="name">Ten</label>
                    <input id="name" class="form-control"  name="name" placeholder="Ten "   />
                    <div style="margin-top:10px">
                     @error('name')
                         <div class="alert alert-danger">{{ $message }}</div>
                     @enderror 
                    </div> 
                </div>
                <div class="form-group ">
                    <label for="money">so tien</label>
                    <input id="money" class="form-control" type="number"  name="money" placeholder="money"  />
                    <div style="margin-top:10px">
                     @error('money')
                         <div class="alert alert-danger">{{ $message }}</div>
                     @enderror 
                    </div>
                </div>
                
                <button type="submit" class="btn btn-primary">Them vi</button>
                <form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

</div>
@stop