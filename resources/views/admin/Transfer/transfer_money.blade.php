@extends('admin.layout.index_metronic')
@section('metronic')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Chuyển tiền

                </h1>
            </div>
            <div class="col-lg-4" style="padding-bottom:120px;">
                <form action="{{route('postTransfer')}}" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="code">Tên tài khoản</label>
                        <input id="code" class="form-control " name="code"  placeholder="Nhập số tài khoản " />
                        <div style="margin-top:10px" > 
                            @error('code') 
                                <div class="alert alert-danger">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                        
                    </div>
                    <div class="form-group ">
                        <label for="money">Nhập số tiền</label>
                        <input id="money" class="form-control  " type="number" name="money"  placeholder="Nhập số tiền" />
                        <div style="margin-top:10px">
                            @error('money')
                                <div class="alert alert-danger">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                      
                    </div>
                    <div class="form-group">
                    </div>
                    <button type="submit" class="btn btn-primary">Chuyển tiền</button>
                    <a class="btn btn-primary" href="{{route('list_user')}}">Back</a>

                    <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
@stop
