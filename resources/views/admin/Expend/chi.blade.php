@extends('admin.layout.index_metronic')
@section('metronic')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Hiển thị danh sách các khoản chi
                </h1>
                @if($type_chi->count()>=1)
                <h4>
                    <a class="btn btn-success" href="{{route('excel')}}">Export Data excel</a>
                </h4>

            </div>
            <table class="table table-bordered">
                <thead>
                    <tr align="center">
                        <th style="text-align: center;">STT</th>
                        <th style="text-align: center;">Name</th>
                        <th style="text-align: center;">Danh muc</th>
                        <th style="text-align: center;">So tien dang co</th>
                        <th style="text-align: center;">So tien sau khi su dung </th>
                        <th style="text-align: center;">So tien </th>
                        <th style="text-align: center;">Ngay</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($type_chi as $chi)
                    <tr class="odd gradeX" align="center">
                        <td>{{$loop->iteration}}</td>
                        <td>{{$chi->name}}</td>
                        <td>{{$chi->type == 0  ? 'CHI' : 'THU' }}</td>
                        <td>
                        {{number_format($chi->money_before,0,',','.')}} VNĐ
                        </td>
                        <td>
                        {{number_format($chi->money_after,0,',','.')}} VNĐ
                        </td>
                        <td>
                        {{number_format($chi->value,0,',','.')}} VNĐ
                        </td>
                        <td>{{$chi->created_at}}</td>
                    </tr> 
                    @endforeach
                </tbody>
            </table>
            @else
            <div>
                <h3 style="text-align: center;color: black">
                    Không có dữ liệu
                </h3>

            </div>
            @endif
        </div>



    </div>
</div>
@stop
