@extends('admin.layout.index_metronic')
@section('metronic')
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Hiển thị danh sách giao dịch theo thời gian 
        </h1>
      
        <h4>
          
           <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 In báo cáo theo tháng<span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="{{route('listDayForMonth', ['month' => 1])}}">Tháng 1</a></li>
                  <li><a href="{{route('listDayForMonth', ['month' => 2])}}">Tháng 2</a></li>
                  <li><a href="{{route('listDayForMonth', ['month' => 3])}}">Tháng 3</a></li>
                  <li><a href="{{route('listDayForMonth', ['month' => 4])}}">Tháng 4</a></li>
                  <li><a href="{{route('listDayForMonth', ['month' => 5])}}">Tháng 5</a></li>
                  <li><a href="{{route('listDayForMonth', ['month' => 6])}}">Tháng 6</a></li>
                  <li><a href="{{route('listDayForMonth', ['month' => 7])}}">Tháng 7</a></li>
                  <li><a href="{{route('listDayForMonth', ['month' => 8])}}">Tháng 8</a></li>
                  <li><a href="{{route('listDayForMonth', ['month' => 9])}}">Tháng 9</a></li>
                  <li><a href="{{route('listDayForMonth', ['month' => 10])}}">Tháng 10</a></li>
                  <li><a href="{{route('listDayForMonth', ['month' => 11])}}">Tháng 11</a></li>
                  <li><a href="{{route('listDayForMonth', ['month' => 12])}}">Tháng 12</a></li>
                 
                
                </ul>
                
            </div>  
            @if($type_day->count()>=1)
            <div class="btn-group">
                <a  style="margin-left: 10px" class="btn btn-primary" href="{{route('day', ['day' => $month])}}">Export Data excel
                </a>
            </div> 
        </h4>
    
      </div>
      <table class="table table-bordered">
        <thead>
         <tr align="center">
          <th style="text-align: center;">STT</th>
          <th style="text-align: center;">Giao dịch</th>
          <th style="text-align: center;">Danh muc</th>
          <th style="text-align: center;">So tien hiện tại</th>
          <th style="text-align: center;">So tien sau khi su dung </th>
          <th style="text-align: center;">So tien </th>
          <th style="text-align: center;">Thời gian</th>
         
        </tr>
      </thead>
      <tbody>
      @foreach($type_day as $day)
       <tr class="odd gradeX" align="center">
        <td>{{$loop->iteration}}</td>
        <td>{{$day->name}}</td>
        <td>{{$day->type == 1 ? 'Chi' : 'Thu' }}</td>
        <td>{{number_format($day->money_before,0,',','.')}} VNĐ</td>
        <td>{{number_format($day->money_after,0,',','.')}} VNĐ</td>
        <td>{{number_format($day->value,0,',','.')}} VNĐ</td>
        <td>{{$day->created_at}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
   @else
   <div >
     <h3 style="text-align: center;color: black">
       Không có dữ liệu
     </h3>
   </div>
   @endif
</div>


{{$type_day->links()}}
</div>
</div>
@stop