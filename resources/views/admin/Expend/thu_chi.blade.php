@extends('admin.layout.index_metronic')
@section('metronic')
<div id="page-wrapper" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Giao dịch
                  
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @if(session('thongbao'))
                <div class="alert alert-success" >
                    {{session('thongbao')}}
                </div>
                @endif
                <form action="{{route('store')}}" method="POST">
                @csrf()
                 <div class="form-group">
                    <label for="name">Ten</label>
                    <input id="name" class="form-control"  name="name" placeholder="Ten "   />
                    <div style="margin-top:10px" >
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                     
                 </div>
                 <div class="form-group" >
                 	<label for="type" >Danh mục</label>
                 	<select id="type" required name="type" class="form-control">
                 		<option value="1">Thu</option>
                 		<option value="0">Chi</option>
                 	</select>
                 </div>
               		<div class="form-group">
                    <label for="value">Sô tiền</label>
                    <input id="value" class="form-control" name="value"  />
                    <div style="margin-top:10px" >
                        @error('value')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    
                 </div>
                
                <button type="submit" class="btn btn-primary">Giao dịch</button>
                <a class="btn btn-primary" href="{{route('list_user')}}">Back</a>
                
                <form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

</div>
@stop