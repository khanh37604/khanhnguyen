@extends('admin.layout.index_metronic')
@section('metronic')
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">
          Hiển thị danh sách các loại thu 
        </h1>
        @if($type_thu->count()>=1)
        <h4>
          <a class="btn btn-success" href="{{route('excels')}}">Export Data excel</a>
                
        </h4>
       
      </div>
      <table class="table table-bordered">
        <thead>
         <tr align="center">
          <th style="text-align: center;">STT</th>
          <th style="text-align: center;">Name</th>
          <th style="text-align: center;">Danh muc</th>
          <th style="text-align: center;">Số tiền hiện tại</th>
          <th style="text-align: center;">Số tiền sau khi sử dụng</th>
          <th style="text-align: center;">Số tiền thu </th>
          <th style="text-align: center;">Thời gian</th>
         
        </tr>
      </thead>
      <tbody>
      @foreach($type_thu as $thu)
       <tr class="odd gradeX" align="center">
        <td>{{$loop->iteration}}</td>
        <td>{{$thu->name}}</td>
        <td>{{$thu->type == 1 ? 'THU' :'CHI' }}</td>
        <td>{{number_format($thu->money_before,0,',','.')}} VNĐ</td>
        <td>{{number_format($thu->money_after,0,',','.')}} VNĐ</td>
        <td>{{number_format($thu->value,0,',','.')}} VNĐ</td>
        <td>{{$thu->created_at}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @else
  <div >
    <h3 style="text-align: center;color: black">
      Không có dữ liệu
    </h3>
  </div>
  @endif
</div>



</div>
</div>
@stop