<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Register</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
<link href="{{asset('admin_css/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('admin_css/datepicker3.css')}}" rel="stylesheet">
<link href="{{asset('admin_css/styles.css')}}" rel="stylesheet">

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js')}}"></script>
<![endif]-->

</head>

<body>
    
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div style="text-align:center" class="panel-heading">Đăng  kí</div>
                <div class="panel-body">
                    <form role="form" method="post" action="{{route('store_register')}}">
                       {{csrf_field()}}
                        <fieldset>
                            <div class="form-group ">
                                <label for="name">Name</label>
                                <input id="name" class="form-control " " placeholder="Name" name="name" type="text" >
                                <div style="margin-top:10px" >
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="email">Email</label>
                                <input id="email" class="form-control" placeholder="E-mail"  name="email" type="email" autofocus="">
                                <div style="margin-top:10px">
                                @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                </div>
                               
                            </div>
                            <div class="form-group">
                                <label for="confirm_password">Password</label>
                                <input id="confirm_password" class="form-control" placeholder="Password" name="password" type="password" value="">
                                <div style="margin-top:10px">
                                @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="confirm_passwords" >Nhập lại Password</label>
                                <input id="confirm_passwords" class="form-control" placeholder="Password" name="confirm_password" type="password" value="">

                                <div style="margin-top:10px">
                                @error('confirm_password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                </div>
                            </div>
                            <div style="text-align:center">
                                 <button type="submit" class="btn btn-primary">Đăng kí</button>
                                
                            </div>
                                 
                        </fieldset>
                    </form>
                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->    
    
        
    <script src="http://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
    <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
    {!! Toastr::message() !!}
    <script src="{{asset('admin_css/js/jquery-1.11.1.min.js')}}"></script>
    <script src="{asset('admin_css/js/bootstrap.min.js')}}"></script>
    <script src="{asset('admin_css/js/chart.min.js')}}"></script>
    <script src="{asset('admin_css/js/chart-data.js')}}"></script>
    <script src="{asset('admin_css/js/easypiechart.js')}}"></script>
    <script src="{asset('admin_css/js/easypiechart-data.js')}}"></script>
    <script src="{asset('admin_css/js/bootstrap-datepicker.js')}}"></script>
    <script>
        !function ($) {
            $(document).on("click","ul.nav li.parent > a > span.icon", function(){        
                $(this).find('em:first').toggleClass("glyphicon-minus");      
            }); 
            $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
        }(window.jQuery);

        $(window).on('resize', function () {
          if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
        })
        $(window).on('resize', function () {
          if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
        })
    </script>   
</body>

</html>
