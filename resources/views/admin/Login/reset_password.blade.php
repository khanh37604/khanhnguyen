

@extends('admin.layout.index_metronic')
@section('metronic')
<div id="page-wrapper" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <small>Đổi mật khẩu</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">   
                <form action="{{route('resetPassword')}}" method="POST" >
                 @csrf()
                 <div class="form-group">
                    <label for="curr_password" >Nhập  Password</label>
                    <input id="curr_password" class="form-control" type="password" name="curr_password" s/>
                    <div style="margin-top:10px" >
                     @error('curr_password')
                         <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="new_password" >Nhập Password mới</label>
                    <input id="new_password" class="form-control" name="new_password" type="password"   />
                   <div style="margin-top:10px" >
                     @error('new_password')
                         <div class="alert alert-danger">{{ $message }}</div>
                     @enderror
                   </div>
                     
                </div>
                <div class="form-group">
                    <label for="confirm_password" >Nhập lại password </label>
                    <input id="confirm_password" class="form-control"  name="confirm_password" type="password"   />
                    <div style="margin-top:10px">
                    @error('confirm_password')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                    @enderror
                    </div>
                </div>
                

            <button type="submit" class="btn btn-primary">Đổi mật khẩu</button>
            <a  class="btn btn-primary" href="{{route('list_user')}}">Back</a>
            <form>
            </div>
        </div>

    </div>

</div>


</div>
@stop