<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Wallet;
use App\Expend;
class Wallet extends Model
{
	protected $table = 'wallets' ;
	protected $fillable =['name','money','user_id'];

	
	public function user()
	{
		return $this->belongsTo(User::class);
		
	}
	public function expends()
	{
		return $this->hasMany(Expend::class);
	}
}
