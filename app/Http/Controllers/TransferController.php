<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
Use App\Wallet;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TransferWallet;
class TransferController extends Controller
{
    public function __construct() {
        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.
        header('Access-Control-Allow-Origin: *');      
    }
    public function getTransfer()

    {

        return view('admin.Transfer.transfer_money');
        
    }

    public function postTransfer(TransferWallet $request)
    
    {
        
         DB::beginTransaction();
         try {
                $value = $request->money;
                $myWallet = Auth::user()->wallet;
                $user = User::where('code', $request->code)->first();
                $wallet = $user->wallet;

                $myWallet->money -= $value;
                $myWallet->save();
                $wallet->money += $value;
                $wallet->save();
                DB::commit();
              
                 \Toastr::success('Bạn đã chuyển tiền thành công', 'Thông báo', ["positionClass" => "toast-top-center"]);
                return redirect()->route('getTransfer');
            } catch (\Exception $e) {
                
                DB::rollback();
             \Toastr::warning('Tài khoản này chưa tạo ví nên chưa chuyển tiền', 'Thông báo', ["positionClass" => "toast-top-center"]);
                return redirect()->back();
        }
    }
}
