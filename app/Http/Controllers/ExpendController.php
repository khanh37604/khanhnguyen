<?php

namespace App\Http\Controllers;

use App\Expend;
use App\Exports\DayExport;
use App\Exports\ExpendExport;
use App\Exports\ExpendExports;
use App\Http\Requests\ExpendRequest;
use App\User;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ExpendController extends Controller
{
    public function __construct() {
        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.
        header('Access-Control-Allow-Origin: *');      
    }
    public function create()

    {

        return view('admin.Expend.thu_chi');

    }

    public function store(ExpendRequest $request)

    {
        $ex = Auth::user()->wallet;
        $data = $request->only('name', 'type', 'value', 'money_before', 'money_after', 'wallet_id');
        $data['money_before'] = $ex->money;
        $moneyCurrent = $ex->money;
        if ($request->type == 0 ) {
            
            $data['money_after'] = $moneyCurrent -= $request->value;
           
            $ex->update([
                'money' => $moneyCurrent,
            ]);
        } else {
            $data['money_after'] = $moneyCurrent += $request->value;
           
            $ex->update([
                'money' => $moneyCurrent,
            ]);
            
        }
        $data['wallet_id'] = $ex->id;
        Expend::create($data);
        \Toastr::success('Ban da giao dich thanh cong', 'Thong bao', ["positionClass" => "toast-top-center"]);
        return redirect()->route('store');
       
    }

    public function listchi()

    {

        $type_chi = Expend::where('type', 0)->get();

        return view('admin.Expend.chi', compact('type_chi'));

    }

    public function listthu()

    {

        $type_thu = Expend::where('type', 1)->get();

        return view('admin.Expend.thu', compact('type_thu'));

    }

    public function excel()

    {

        return Excel::download(new ExpendExport, 'Danh_sach_chi.xlsx');

    }

    public function excels()

    {

        return Excel::download(new ExpendExports, 'Danh_sach_thu.xlsx');

    }

    public function day(Request $request, $month)

    {
        //xử lý xuất file excel,truyền biến $month để in theo tháng
        return Excel::download(new DayExport($month), 'Danh_sach_theo_thang.xlsx');
    }

    public function listDayForMonth(Request $request, $month)

    {
        
        $type_day = Expend::whereMonth('created_at', '=', Carbon::now()->month($month))->paginate('4');

        return view('admin.Expend.list_1', compact('type_day', 'month'));
    }

}
