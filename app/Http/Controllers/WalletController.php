<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wallet;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\WalletRequest;
use Illuminate\Support\Facades\DB;
class WalletController extends Controller
{
    public function __construct() {
        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.
        header('Access-Control-Allow-Origin: *');      
    }
    public function index()

    {
        $wallet = auth()->user()->wallet ;
        return view('admin.Wallet.list_wallet',compact('wallet'));
               	
    }

    public function create()

    {

        return view('admin.Wallet.create_wallet');

    }
    public function store(WalletRequest $request)

    {
       if(!auth()->user()->wallet) {
           $request->merge(['user_id'=>auth()->id()]);
           Wallet::create([
             'name' => $request->name,
             'money' => $request->money,
             'user_id' => $request->user_id
         ]);
         
           \Toastr::success('Bạn đã thêm thành công', 'Thông báo', ["positionClass" => "toast-top-center"]);
           return redirect()->route('list_wallet');
        }
        
      
     
    }

    public function edit($id)
    {
       $wallet = Wallet::find($id);

       return view ('admin.Wallet.edit_wallet',compact('wallet'));

    }
    public function update(WalletRequest $request,$id)

    { 
    		//
       $wallet = Auth::user()->wallet;
       $wallet->update([
            'money' => $request->money,
            'name' => $request->name,
        ]);
        \Toastr::success('Sửa thông tin ví thành công', 'Thông báo', ["positionClass" => "toast-top-center"]);
        return redirect()->route('edit_wallet',$id);
 
    }	

    public function destroy($id)

    {

       Wallet::destroy($id);
       \Toastr::warning('Xóa ví thành công', 'Thông báo', ["positionClass" => "toast-top-center"]);
       return redirect()->route('list_wallet');
   
    }  
    
}