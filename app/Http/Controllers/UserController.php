<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditUser;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function __construct() {
        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.
        header('Access-Control-Allow-Origin: *');      
    }
    public function index()

    {

        $user = Auth::user();

        return view('admin.User.list_user', compact('user'));
    }

    public function create()

    {

        return view('admin.User.create_user');

    }

    public function edit($id)

    {

        $user = User::find($id);

        return view('admin.User.edit_user', compact('user'));

    }

    public function update(EditUser $request, $id)

    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->birthday = $request->birthday;
    
        if ($request->hasfile('images')) {
            $DelImages = $user->images;
            Storage::delete('/public/' . $DelImages);
            $user['images'] = $request->file('images')->store('avatar', 'public');
        }
        $user->save();
        \Toastr::success('Thay đổi thông tin thành công', 'Thông báo', ["positionClass" => "toast-top-center"]);

        return redirect()->route('list_user');

    }

    public function view($id)

    {

        $user = User::find($id);
        return view('admin.User.view', compact('user'));

    }

}