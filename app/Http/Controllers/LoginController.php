<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\ResetPassword;
use DB;
use Session;
class LoginController extends Controller
{
    public function __construct() {
        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.
        header('Access-Control-Allow-Origin: *');      
    }
    public function getLogin()

    {

        return view('admin.Login.login_metronic');
        
    }

    public function postLogin(LoginRequest $request)

    {

    	$data= [
    		'email' => $request->email,
    		'password' => $request->password
    	];
        if ($request->remember = 'Remember Me' ) {
            $remember = true;
        }
        else
           {
            $remember =false;
           } 
    	if (Auth::attempt($data,$remember)) {
    		return  redirect()->route('list_user');
    	}
    	else
    	{
            \Toastr::error('Tài khoản hoặc mật khẩu không chính xác', 'Thông báo', ["positionClass" => "toast-top-center"]);
    		return  redirect()->route('get_login');
        }
        
    }

     public function create()

    {

        return view('admin.Login.register');
        
    }

    public function store(UserRequest $request)

    { 

    	$user = new User;
    	$user->name= $request->name;
    	$user->email= $request->email;
    	$user->code = mt_rand(0,10000000000);
        $user->password= Hash::make($request->password);
        $user->password = Hash::make($request->confirm_password);
        $user->save();
        Toastr()->info('mess  age', 'title', ['options']);
        return redirect()->route('home');
        
    }
    public function home()
    {
        return view('admin.layout.trangchu_metronic');
    }
    public function logout()

    {
        Auth::logout();
        return redirect('/login');
    }
    
    public function reset()
    {
        return view('admin.Login.reset_password');
    }
    public function resetpassword(ResetPassword $request)
    {
        
            $curr_password = $request->curr_password;
            $new_password  = $request->new_password;
        if(!Hash::check($curr_password,Auth::user()->password)){
        \Toastr::error('Doi mat khau that bai', 'Thong bao', ["positionClass" => "toast-top-center"]);
        return redirect()->route('reset');
        }
        else{
            $request->user()->fill(['password' => Hash::make($new_password)])->save();
            \Toastr::success('Doi mat khau thanh cong', 'Thong bao', ["positionClass" => "toast-top-center"]);
            return redirect()->route('reset');
            }
    }
    
}
