<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Wallet;

class WalletRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'money'=> 'required|numeric|min:0'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Ban chua nhap name',
            'money.required' => 'Ban chua nhap gia tien' ,
            'money.numeric' => 'Ban chua nhap dung dinh dang',
            'money.min' => 'Số tiền nhập vao không được âm'
        ];
    }
}
