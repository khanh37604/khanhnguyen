<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;
class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:5|max:15|',
            'confirm_password' => 'required|same:password'
        ];
    }
     public function messages()
    {
        return [
              'name.required' => 'Ban chua nhap tên tài khoản',
              'email.required'=>'Tài khoản email không được để trống!',
              'email.email'=>'Tài khoản không đúng định dạng!',
              'email.unique' => 'Email đã tồn tại',
              'password.required'=>'Mật khẩu không được để trống!',
              'password.min'=>'Mật khẩu phải ít nhất 5 ký tự!',
              'password.max'=>'Mật khẩu nhiều nhất 15 ký tự!',
              'confirm_password.required' => 'Bạn chưa nhập lại mật khẩu',
              'confirm_password' => 'Bạn chưa nhập lại mật khẩu',
              'confirm_password.same' => 'Mật khẩu chưa khớp'
        ];
    }
}
