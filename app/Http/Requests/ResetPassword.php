<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'curr_password' => 'required:sam:password',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password'
        ];
    }
    public function messages()
    {
        return [
            'curr_password.required' => 'Bạn chưa nhập mật khẩu',
            'curr_password.same' => 'Mật khẩu bạn nhập chưa chính xác',
            'new_password.required' => 'Bạn chưa nhập mật khẩu mới',
            'new_password.min' => 'Mật khẩu phải nhiều hớn 6 kí tự',
            'confirm_password.required' => 'Bạn chưa nhập mật khẩu',
            'confirm_password.same' => 'Mật khẩu chưa trùng nhau'
        ];
    }

}
