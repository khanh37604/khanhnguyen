<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;
use App\Wallet;
use App\Rules\ValiTransfer;

class TransferWallet extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|exists:users,code',
            'money' => ['required','numeric','min:0',new ValiTransfer($this->money)]
        ];
    }
    public function messages()
    {
        return [
            'code.required' => 'Tài khoản không được để trống',
            'code.exists'   => 'Tài khoản này không tồn tại',
            'money.required' => 'Bạn chưa nhập số tiền cần chuyển',
            'money.numeric' => 'Vui lòng nhập số',
            'money.min' => 'So tien chuyen khong duoc am'
            
        ];
    }
}
