<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Expend;
use App\Rules\Extend;
class ExpendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>'required',
            'value' =>['required','numeric','min:0',new Extend($this->value)]
            
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Name khong duoc de trong',
            'value.required' => 'Ban chua nhap so tien',
            'value.numeric' => 'Ban chua nhap dung dinh dang',
            'value.min'     => 'Số tiền không được âm'
        ];
    }
}
