<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Wallet;

class ViewDelvi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)

    {
       if (auth()->user()->wallet) 
       {
            \Toastr::warning('Tài khoản đã tồn tại ví', 'Thông báo', ["positionClass" => "toast-top-center"]);
            return redirect()->route('list_wallet');
              
       }

       
       return $next($request);
       
    }

}
