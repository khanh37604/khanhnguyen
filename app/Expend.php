<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Wallet;
class Expend extends Model
{
    protected $table = 'expends';
    protected $fillable =['name','type','money_before','money_after','value','wallet_id'];
    public function wallet()
    {
    	return $this->belongsTo(Wallet::class);
    }
}
