<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Expend;
class ExpendExports implements FromCollection,WithHeadings

{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
          return Expend::query()->where('type', 1)->get(['id','name','type' , 'money_before','money_after','value','created_at']);
    }
    public function headings() :array {
    	return ["STT", "Tên ", "Danh muc", "Money_before","Money_afer","Value","Created_at"];
    }
    
}
