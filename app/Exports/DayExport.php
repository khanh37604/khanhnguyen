<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Expend;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\WithHeadings;
class DayExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
   
    //thêm biến $month để xử lý tháng

   protected $month;
   public function __construct($month)
   {
    $this->month = $month;
   }
    public function collection()
    {   

        
        return Expend::whereMonth('created_at', '=',Carbon::now()->month($this->month))->get(['name','type', 'money_before','money_after','value','created_at']);
         }

    public function headings() : array{
    	return ["Name","Danh muc","Số tiền hiện tại","Số tiền sau khi sử dụng","Số tiền","Created_at"];
    }
}
